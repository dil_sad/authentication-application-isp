import 'package:flutter/material.dart';
import 'package:authentication_app/screens/home/icons.dart';
import 'package:authentication_app/screens/home/widgets/svg_asset.dart';
import 'package:get/get.dart';
import 'package:authentication_app/widgets/small_card.dart';
import 'package:url_launcher/url_launcher.dart';

class Restricted extends StatefulWidget {
  const Restricted({super.key});

  @override
  State<Restricted> createState() => _RestrictedState();
}

class _RestrictedState extends State<Restricted> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff121421),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 28,
                right: 18,
                top: 36,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(360),
                    onTap: () {
                      Get.toNamed('/Home');
                    },
                    child: const SizedBox(
                      height: 35,
                      width: 35,
                      child: Center(
                        child: SvgAsset(
                          assetName: AssetName.back,
                          height: 24,
                          width: 24,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 55),
                  const Text("Restricted",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 34,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            const SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28),
              child: GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 19,
                    mainAxisExtent: 125,
                    mainAxisSpacing: 19),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/restricted%2FUAT%20%20Relase%20note%20and%20QA%20Summary%20-%20TREK%20SEC.xlsx?alt=media&token=50cd0801-7b42-400d-956d-dce029d8a447');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "UAT Release note and QA Summary - TREK SEC",
                    gradientStartColor: const Color(0xff13DEA0),
                    gradientEndColor: const Color(0xff06B782),
                  ),
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/restricted%2FE-Banking%20Application%20SRS%E2%80%93%20TREK%20SEC-%20ISO%209001.docx?alt=media&token=d757e51f-d69d-4cb8-b147-592d8a357ac7');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "E-Banking Application SRS – TREK SEC - ISO 9001",
                    gradientStartColor: const Color(0xffFC67A7),
                    gradientEndColor: const Color(0xffF6815B),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
