import 'package:flutter/material.dart';
import 'package:authentication_app/screens/home/icons.dart';
import 'package:authentication_app/screens/home/widgets/svg_asset.dart';
import 'package:get/get.dart';
import 'package:authentication_app/widgets/small_card.dart';
import 'package:url_launcher/url_launcher.dart';

class Public extends StatefulWidget {
  const Public({super.key});

  @override
  State<Public> createState() => _PublicState();
}

class _PublicState extends State<Public> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff121421),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 28,
                right: 18,
                top: 36,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(360),
                    onTap: () {
                      Get.toNamed('/Home');
                    },
                    child: const SizedBox(
                      height: 35,
                      width: 35,
                      child: Center(
                        child: SvgAsset(
                          assetName: AssetName.back,
                          height: 24,
                          width: 24,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 80),
                  const Text("Public",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 34,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            const SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28),
              child: GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 19,
                    mainAxisExtent: 125,
                    mainAxisSpacing: 19),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/public%2FDisclaimer.docx?alt=media&token=37595b6d-9fe7-4069-9aa5-8ef388652abf');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "Disclaimer",
                    gradientStartColor: const Color(0xff13DEA0),
                    gradientEndColor: const Color(0xff06B782),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
