import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'icons.dart';
import 'widgets/discover_small_card.dart';
import 'widgets/svg_asset.dart';

class DiscoverPage extends StatefulWidget {
  const DiscoverPage({Key? key}) : super(key: key);

  @override
  State<DiscoverPage> createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  final password = TextEditingController();
  bool _validate = false;

  @override
  void dispose() {
    password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff121421),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 28,
                right: 18,
                top: 36,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Home",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 34,
                          fontWeight: FontWeight.bold)),
                  InkWell(
                    borderRadius: BorderRadius.circular(360),
                    onTap: () {},
                    child: const SizedBox(
                      height: 35,
                      width: 35,
                      child: Center(
                        child: SvgAsset(
                          assetName: AssetName.search,
                          height: 24,
                          width: 24,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28),
              child: GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 19,
                    mainAxisExtent: 125,
                    mainAxisSpacing: 19),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  DiscoverSmallCard(
                    onTap: () {
                      showPrivateAlert();
                    },
                    title: "Private",
                    gradientStartColor: const Color(0xff13DEA0),
                    gradientEndColor: const Color(0xff06B782),
                    icon: Icon(
                      Icons.privacy_tip_outlined,
                      size: 30,
                      color: Colors.grey.shade300,
                    ),
                  ),
                  DiscoverSmallCard(
                    onTap: () {
                      Get.toNamed('/Public');
                    },
                    title: "Public",
                    gradientStartColor: const Color(0xffFC67A7),
                    gradientEndColor: const Color(0xffF6815B),
                    icon: Icon(
                      Icons.public,
                      size: 30,
                      color: Colors.grey.shade300,
                    ),
                  ),
                  DiscoverSmallCard(
                    onTap: () {
                      showRestrictedAlert();
                    },
                    title: "Restricted",
                    gradientStartColor: const Color.fromARGB(255, 22, 119, 255),
                    gradientEndColor: const Color.fromARGB(255, 79, 152, 255),
                    icon: Icon(
                      Icons.block,
                      size: 30,
                      color: Colors.grey.shade300,
                    ),
                  ),
                  DiscoverSmallCard(
                    onTap: () {
                      Get.toNamed('/TodoIntro');
                    },
                    title: "To Do List",
                    icon: Icon(
                      Icons.list_alt_outlined,
                      size: 30,
                      color: Colors.grey.shade300,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  showPrivateAlert() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(
                  20.0,
                ),
              ),
            ),
            contentPadding: const EdgeInsets.only(
              top: 10.0,
            ),
            title: const Text(
              "Private",
              style: TextStyle(fontSize: 24.0),
            ),
            content: SizedBox(
              height: 200,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Enter The Password ",
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: password,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            errorText: _validate ? 'Password Incorrect' : null,
                            hintText: 'Enter Password here',
                            labelText: 'Password'),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 145,
                          height: 60,
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                            onPressed: () {
                              if (password.text == "private123") {
                                Get.toNamed('/Private');
                              } else {
                                setState(() {
                                  _validate = true;
                                });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.black,
                            ),
                            child: const Text(
                              "Submit",
                            ),
                          ),
                        ),
                        Container(
                          width: 145,
                          height: 60,
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red,
                            ),
                            child: const Text(
                              "Cancel",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  showRestrictedAlert() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(
                  20.0,
                ),
              ),
            ),
            contentPadding: const EdgeInsets.only(
              top: 10.0,
            ),
            title: const Text(
              "Restricted",
              style: TextStyle(fontSize: 24.0),
            ),
            content: SizedBox(
              height: 200,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Enter The Password ",
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: password,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            errorText: _validate ? 'Password Incorrect' : null,
                            hintText: 'Enter Password here',
                            labelText: 'Password'),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 145,
                          height: 60,
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                            onPressed: () {
                              if (password.text == "restricted123") {
                                Get.toNamed('/Restricted');
                              } else {
                                setState(() {
                                  _validate = true;
                                });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.black,
                            ),
                            child: const Text(
                              "Submit",
                            ),
                          ),
                        ),
                        Container(
                          width: 145,
                          height: 60,
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red,
                            ),
                            child: const Text(
                              "Cancel",
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Container(
                    //   padding: const EdgeInsets.all(8.0),
                    //   child: const Text('Note'),
                    // ),
                    // const Padding(
                    //   padding: EdgeInsets.all(8.0),
                    //   child: Text(
                    //     'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt'
                    //     ' ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud'
                    //     ' exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
                    //     ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum '
                    //     'dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,'
                    //     ' sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    //     style: TextStyle(fontSize: 12),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
