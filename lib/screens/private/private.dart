import 'package:flutter/material.dart';
import 'package:authentication_app/screens/home/icons.dart';
import 'package:authentication_app/screens/home/widgets/svg_asset.dart';
import 'package:get/get.dart';
import 'package:authentication_app/widgets/small_card.dart';
import 'package:url_launcher/url_launcher.dart';

class Private extends StatefulWidget {
  const Private({super.key});

  @override
  State<Private> createState() => _PrivateState();
}

class _PrivateState extends State<Private> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff121421),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 28,
                right: 18,
                top: 36,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(360),
                    onTap: () {
                      Get.toNamed('/Home');
                    },
                    child: const SizedBox(
                      height: 35,
                      width: 35,
                      child: Center(
                        child: SvgAsset(
                          assetName: AssetName.back,
                          height: 24,
                          width: 24,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 80),
                  const Text("Private",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 34,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            const SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28),
              child: GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 19,
                    mainAxisExtent: 125,
                    mainAxisSpacing: 19),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/files%2FTREK%20SEC%20%20Cumstomer%20Satisfaction%20Survey.docx?alt=media&token=805c71ae-f83e-4b96-b7c0-4dfb436ff8c0');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "TREK SEC Customer Satisfaction Survey",
                    gradientStartColor: const Color(0xff13DEA0),
                    gradientEndColor: const Color(0xff06B782),
                  ),
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/files%2FTREK%20SEC%20Interested%20Parties.xlsx?alt=media&token=6e191e6b-2dbc-4177-8f71-1da8bbc02585');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "TREK SEC Interested Parties",
                    gradientStartColor: const Color(0xffFC67A7),
                    gradientEndColor: const Color(0xffF6815B),
                  ),
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/files%2FTREK%20SEC%20%20Identification%20and%20Tracebility.docx?alt=media&token=320b7a6b-5369-4718-a707-2ab8d6747cd0');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "TREK SEC Identification and Traceability",
                    gradientStartColor: const Color(0xffFFD541),
                    gradientEndColor: const Color(0xffF0B31A),
                  ),
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/files%2FTREK%20SEC%20Internal%20Audit%20Finindings.xlsx?alt=media&token=c36e61a0-a19d-47bf-88df-c585d9c19a39');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "TREK SEC Internal Audit Findings",
                    gradientStartColor: const Color.fromARGB(255, 34, 85, 255),
                    gradientEndColor: const Color.fromARGB(255, 78, 119, 254),
                  ),
                  SmallCard(
                    onTap: () async {
                      final Uri url = Uri.parse(
                          'https://firebasestorage.googleapis.com/v0/b/flutter-authentication-7e004.appspot.com/o/files%2FTREK%20SEC%20ISO%209001%202015%20Internal%20Audit%20%20Checklist.xlsx?alt=media&token=e12e710c-6b75-4e0c-8464-356a1575bd18');
                      await launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    },
                    title: "TREK SEC ISO 9001 2015 Internal Audit Checklist",
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
