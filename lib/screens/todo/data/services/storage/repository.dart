import 'package:authentication_app/screens/todo/data/provideData.dart/taskProvide.dart';
import 'package:authentication_app/models/task.dart';

class TaskRepository {
  TaskProvider taskProvider;
  TaskRepository({required this.taskProvider});

  List<Task> readTasks() => taskProvider.readTasks();
  void writeTasks(List<Task> tasks) => taskProvider.writeTasks(tasks);
}
