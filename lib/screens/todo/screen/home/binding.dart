import 'package:get/get.dart';
import 'package:authentication_app/screens/todo/controller/homecontroller.dart';
import 'package:authentication_app/screens/todo/data/provideData.dart/taskProvide.dart';
import 'package:authentication_app/screens/todo/data/services/storage/repository.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Homecontroller(
        taskRepository: TaskRepository(taskProvider: TaskProvider())));
  }
}
