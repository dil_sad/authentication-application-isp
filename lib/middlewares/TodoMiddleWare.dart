import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:authentication_app/main.dart';
import 'package:authentication_app/screens/todo/core/keys.dart';

class IntroMeddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    if (preferences!.getBool(DISPLAYINTRO) != null)
      return RouteSettings(name: '/Todo');
  }
}
